package INF101.lab2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    List<FridgeItem> items = new ArrayList<>();
    int capacity = 20;

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return capacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (items.size() < capacity) {
            items.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (items.contains(item)) { // if (list.contains(entry)): <=> if entry in list {}
            items.remove(item);
        }    
        else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        items.clear(); // .clear() deletes everything in the list   
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<>();
        for (FridgeItem item : items) {
            if (item.getExpirationDate().isBefore(LocalDate.now())) {
            expiredItems.add(item);
            }
        }
        items.removeAll(expiredItems);
        return expiredItems;
    }

}
